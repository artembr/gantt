import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faTasks, faMinusSquare, faPlusSquare, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-project-name',
  templateUrl: './project-name.component.html',
  styleUrls: ['./project-name.component.css']
})
export class ProjectNameComponent implements OnInit {
  projectIcon = faTasks;
  addNewIcon = faPlus;
  cancelIcon = faTimes;
  @Input() project: Project;
  @Output() addProject: EventEmitter<Project> = new EventEmitter<Project>();
  @Output() removeProject: EventEmitter<Project> = new EventEmitter<Project>();
  offset = '';
  show = false;
  constructor() { }

  ngOnInit() {
    this.decorateName();
  }

  decorateName() {
    if (!this.project) {
      return;
    }

    let offset = ' ';
    for (let i = 0; i < this.project.level; i++) {
      offset += '&emsp;';
    }
    this.offset = offset;
  }

  onIconClick() {
    this.project.show = !this.project.show;
  }

  setIcon(p: Project) {
    if (p.children.length) {
      this.projectIcon = this.project.show ? faMinusSquare : faPlusSquare;
    } else {
      this.projectIcon = faTasks;
    }

    return this.projectIcon;
  }
}
