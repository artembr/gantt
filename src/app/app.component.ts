import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data-service/data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewProjectModalComponent } from './components/new-project-modal/new-project-modal.component';
import { RemoveProjectModalComponent } from './components/remove-project-modal/remove-project-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  rootId: string;
  projects: Project[];
  title = 'AngularGantt';

  constructor(private dataSource: DataService, private modalService: NgbModal) {
    this.projects = [];
  }

  ngOnInit() {
    this.dataSource.getData().subscribe(data => {
      this.parse(data, null);
    });
  }

  open(project: Project, action: string) {
    const ref = this.modalService.open( action === 'add' ? NewProjectModalComponent : RemoveProjectModalComponent, { backdrop: 'static' });
    const instance = ref.componentInstance;
    instance.project = project;
    instance.projects = this.projects;
  }

  getYears(p: Project[]): number[] {
    if (p.length === 0) {
      return [];
    }

    const years: number[] = [];
    const start = p.reduce((p1, p2) => p1.start < p2.start ? p1 : p2).start.getFullYear();
    const end = p.reduce((p1, p2) => p1.end > p2.end ? p1 : p2).end.getFullYear();

    if (end < start) {
      return [];
    }

    for (let y = start; y < end; y++) {
      years.push(y);
    }

    if (start !== end) {
      years.push(end);
    }

    return years;
  }

  getQuarts() {
    return ['I', 'II', 'III', 'IV'];
  }

  parse(obj, parent: Project) {
    const id = obj['id'];
    const name = obj['project'];
    const start = new Date(obj['start']);
    const end = new Date(obj['end']);
    const children = obj['children'] ? obj['children'] : [];
    const level = parent ? parent.level + 1 : 0;

    if (!parent) {
        this.rootId = id;
    }
    const p: Project = {
      id: id,
      project: name,
      start: start,
      end: end,
      children: children.length > 0 ? obj['children'].map(data => data.id) : [],
      level: level,
      show: parent === null,
      parentId: parent ? parent.id : null
    };

    this.projects.push(p);
    children.forEach((child) => this.parse(child, p));
  }

  getProjectById(id: number): Project {
    return this.projects.filter(p => p.id === id)[0];
  }
}
