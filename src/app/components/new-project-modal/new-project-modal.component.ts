import { Component, Input, AfterContentChecked } from '@angular/core';
import { NgbActiveModal, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { DateFormatterService, isNumber, toInteger } from '../../services/date-formatter/date-formatter.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-project-modal',
  templateUrl: './new-project-modal.component.html',
  styleUrls: ['./new-project-modal.component.css'],
  providers: [{ provide: NgbDateParserFormatter, useClass: DateFormatterService }]
})

export class NewProjectModalComponent implements AfterContentChecked {
  faCalendar = faCalendar;
  @Input() projects: Project[];
  @Input() project: Project;
  name = '';
  start = null;
  end = null;
  startDate: Date;
  endDate: Date;

  buttonDisabled = true;

  constructor(public activeModal: NgbActiveModal) { }

  ngAfterContentChecked() {
    if (this.name && this.validDates(this.start, this.end)) {
      this.buttonDisabled = false;
    } else {
      this.buttonDisabled = true;
    }
  }

  validDates(d1: any, d2: any): boolean {
    d1 = this.getDate(d1);
    d2 = this.getDate(d2);

    if (d1 && d2 && d2 > d1) {
      this.startDate = d1;
      this.endDate = d2;
      return true;
    }

    return false;
  }

  getDate(d: any): Date {
    if (!d) {
      return null;
    }
    if (d instanceof Object) {
      d = d as NgbDateStruct;
      return new Date(d.year, d.month - 1, d.day);
    }

    const dateParts = d.trim().split('.');
    if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
      return new Date(toInteger(dateParts[2]), toInteger(dateParts[1]) - 1, toInteger(dateParts[0]));
    }

    return null;
  }

  save() {
    const id = this.project.children.length ? this.project.children[this.project.children.length - 1] : this.project.id;

    this.projects.forEach(item => {
      if (item.id > id) {
        item.id++;
        item.children.forEach(c => {
          if (c > id) {
            c++;
          }
        });

        if (item.parentId > id) {
          item.parentId++;
        }
      }
    });
    const idx = this.projects.findIndex(item => item.id === id) + 1;

    const p: Project = {
      id: id + 1,
      project: this.name,
      start: this.startDate,
      end: this.endDate,
      children: [],
      level: this.project.level + 1,
      show: false,
      parentId: this.project.id
    };

    this.projects.splice(idx, 0, p);
    this.project.children.push(id + 1);

    this.activeModal.close();
  }

  cancel() {
    this.activeModal.close();
  }

  onKey(name) {
    this.name = name;
  }
}
