import { Injectable, Inject } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  url: string;
  constructor(@Inject('API_URL') apiUrl: string, private http: HttpClient) {
    this.url = apiUrl;
  }

  getUrl() { return this.url; }

  getData(): Observable<any> {
    return this.http.get(this.url);
  }
}
