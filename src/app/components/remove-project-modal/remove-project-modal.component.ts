import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-remove-project-modal',
  templateUrl: './remove-project-modal.component.html',
  styleUrls: ['./remove-project-modal.component.css']
})
export class RemoveProjectModalComponent {
  @Input() projects: Project[];
  @Input() project: Project;

  constructor(public activeModal: NgbActiveModal) { }

  confirm() {
    this.activeModal.close();

    if (!this.project.parentId) {
      this.project.children = [];
      this.projects.splice(1);
      return;
    }

    const idx = this.projects.findIndex(item => item.id === this.project.id);
    const list = [this.project.id];

    this.projects.forEach(p => {
      if (list.indexOf(p.parentId) !== -1) {
        list.push(p.id);
      }
    });
    this.projects.splice(idx, list.length);

    this.projects.forEach(p => {
      p.children = p.children.filter(c => c !== this.project.id);
      if (p.id > this.project.id) {
        p.id -= list.length;
        p.children = p.children.map(c => c -= list.length);
      }
      if (p.parentId > this.project.id) {
        p.parentId -= list.length;
      }
    });
  }

  cancel() {
    this.activeModal.close();
  }
}
