interface Project {
    id: number;
    project: string;
    start: Date;
    end: Date;
    children: number[];
    show: boolean;
    parentId: number;
    level: number;
}
