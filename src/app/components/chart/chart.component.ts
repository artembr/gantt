import { Component, OnInit, Input, ElementRef, OnChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  static one_day = (1000 * 60 * 60 * 24);
  @Input() project: Project;
  @Input() years: number[];
  private width: number;
  private days: number;
  private obs: BehaviorSubject<number>;
  length: number;
  left: number;

  constructor(private el: ElementRef) {
    this.width = 0;
    this.project = null;
    this.obs = new BehaviorSubject<number>(0);
    this.obs.subscribe(() => this.draw(this.width));
  }

  draw(width: number) {
    if (width === 0 || !this.project) {
      return;
    }
    const end = new Date(this.years[this.years.length - 1], 11, 31);
    const start = new Date(this.years[0], 0);

    this.days = this.getDays(this.years);
    this.left = Math.round((this.project.start.getTime() - start.getTime()) / ChartComponent.one_day * width / this.days);
    this.length =  Math.round((this.project.end.getTime() - this.project.start.getTime()) / ChartComponent.one_day * width / this.days);
  }

  getDays(years: number[]): number {
    const end = new Date(years[years.length - 1], 11, 31);
    const start = new Date(years[0], 0);
    return (end.getFullYear() - start.getFullYear() + 1) * 365;
  }

  ngOnInit() {
    this.width = this.el.nativeElement.offsetWidth;
    this.obs.next(this.width);
    setInterval(() => {
      this.draw(this.width);
      if (this.width !== this.el.nativeElement.offsetWidth || this.days !== this.getDays(this.years)) {
        this.width = this.el.nativeElement.offsetWidth;
        this.obs.next(this.width);
      }
    }, 100);
  }
}
