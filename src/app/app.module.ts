import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponent } from './app.component';
import { DataService } from './services/data-service/data.service';
import { HttpClientModule  } from '@angular/common/http';
import { ChartComponent } from './components/chart/chart.component';
import { ProjectNameComponent } from './components/project-name/project-name.component';
import { NewProjectModalComponent } from './components/new-project-modal/new-project-modal.component';
import { RemoveProjectModalComponent } from './components/remove-project-modal/remove-project-modal.component';
import { NgxPopperModule } from 'ngx-popper';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    ProjectNameComponent,
    NewProjectModalComponent,
    RemoveProjectModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    NgxPopperModule
  ],
  providers: [{ provide: 'API_URL', useValue: 'https://ganttchart-f774f.firebaseio.com/projects.json' },
              DataService
  ],
  entryComponents: [NewProjectModalComponent, RemoveProjectModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
